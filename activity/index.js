
// OUTPUT 1
let number = prompt("Enter a number:");

console.log("The number you provided is: " + number);

for(let i=number; i>=0; i--){
	if(i <= 50){
		console.log("The value is at 50. Terminating the loop.");
		break;
	};
	if(i%10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};
	if(i%5 === 0){
		console.log(i);
	}
}

// OUTPUT 2
let word = "supercalifragilisticexpialidocious";
let consonants = "";

console.log(word);

for(let j=0; j<word.length; j++){
	if(word[j]==='a' || word[j]==='e' || word[j]==='i' || word[j]==='o' || word[j]==='u'){
		continue;
	} else{
		consonants += word[j];
	}
};


console.log(consonants);